import cv2
import numpy as np
import os
from pydicom import dcmread
from scipy import ndimage as nd


class DicomModel:

    ZOOM_MAX = 3
    ZOOM_PASS = 0.1
    ZOOM_MIN = 0.1
    ERROR_MIN_MAX = "El valor mínimo no debe ser más grande que el máximo, se mantienen los valores anteriores"

    def __init__(self, path_img_base, path_atlas, path_paciente_medio, path_referencia_atlas):
        # Se carga el paciente
        self.__img_base_old, self.__pixel_len = self.__load_files(path_img_base)
        # Se escala el paciente para que tenga un volumen de 1x1x1 mm³
        self.__img_base = nd.zoom(self.__img_base_old, zoom=self.__pixel_len, order=0)

        # Se definen los rangos mínimos y máximos para poder realizar la visualización
        self.__img_minimum = np.min(self.__img_base.flatten())
        self.__img_maximo = np.max(self.__img_base.flatten())

        self.__valor_minimum = self.__img_minimum
        self.__valor_maximo = self.__img_maximo

        # Se carga el paciente medio
        self.__img_paciente_medio = self.__load_file(path_paciente_medio)
        # Se orienta el paciente medio a la misma orientación que el paciente real
        self.__img_paciente_medio = np.flip(np.flip(self.__img_paciente_medio, axis=1), axis=0)

        # Se carga el atlas y se adapta al paciente medio
        self.__img_atlas = self.__load_file(path_atlas)
        self.__img_atlas = self.__ajustar_atlas()

        self.__referencia_atlas = self.__load_referencia(path_referencia_atlas)

        # Se definen las variables que tendrán toda la información del corregistro
        self.__atlas_corregistrado = None
        self.__paciente_medio_corregistrado = None

        # Se definen la información para poder visualizar el sistema
        self.__corte_actual = 0
        self.__zoom_factor = 1.0

        # Se definen los valores por defecto para visualizar el atlas corregistrado
        self.__color = [255, 0, 0]
        self.__region_atlas_mostrar = 121
        self.__region_atlas_end = 150
        self.__valor_opacidad = 0.2

    def __ajustar_atlas(self):
        img_ref = self.get_paciente_medio()
        atlas = self.get_atlas()
        # Se rota el atlas
        atlas_rotado = nd.rotate(atlas, 90, axes=(1, 0))
        # Se genera un contenedor para poder convertir el segundo corte al primer corte
        atlas_rotado_2 = np.zeros((181, 217, 181), dtype=np.uint8)
        # Se convierte el segundo corte al primero
        for i in range(atlas_rotado.shape[1]):
            atlas_rotado_2[i] = atlas_rotado[:, i, :]
        # Se escala para que tenga el mismo número de cortes que el paciente medio
        atlas_escalado = nd.zoom(atlas_rotado_2, zoom=np.array(img_ref.shape) / np.array(atlas_rotado_2.shape), order=1)
        # Se convierte la parte de atrás en parte de delante
        atlas_rotado_3 = np.flip(atlas_escalado, axis=0)
        return atlas_rotado_3

    # -----------------------------------------------------------------------
    # Get functions
    # -----------------------------------------------------------------------

    def get_paciente_sin_escalar(self):
        return self.__img_base_old

    def get_atlas(self):
        return self.__img_atlas

    def get_paciente(self):
        return self.__img_base

    def get_paciente_medio(self):
        return self.__img_paciente_medio

    def get_paciente_medio_normalizado(self):
        """
        Retorna el paciente medio realizando una normalización a 0 media 1 desviación típica
        :return:
        """
        mean = self.__img_paciente_medio.flatten().mean()
        desv = self.__img_paciente_medio.flatten().std()
        return (self.__img_paciente_medio - mean) / desv

    def get_paciente_normalizado(self):
        """
        Retorna el paciente realizando una normalización a 0 media 1 desviación típica
        :return:
        """
        mean = self.__img_base.flatten().mean()
        desv = self.__img_base.flatten().std()
        return (self.__img_base - mean) / desv

    def get_numero_de_cortes(self):
        return self.__img_base.shape[0]

    def get_corte_actual_rgb(self, mostrar_atlas=False):
        img = self.get_corte_actual_pixels()
        img_gray_scale = self.escalar_imagen(img)
        img_color = cv2.cvtColor(img_gray_scale, cv2.COLOR_GRAY2RGB)

        if mostrar_atlas and self.__atlas_corregistrado is not None:
            img_atlas = self.__get_atlas_color()
            height, width = img_gray_scale.shape
            img_atlas_scaled = cv2.resize(img_atlas, (width, height))
            dst = cv2.addWeighted(img_color, 1 - self.__valor_opacidad, img_atlas_scaled, self.__valor_opacidad, 0)
            return dst
        return img_color

    def __get_atlas_color(self):
        """
        Se encarga de convertir un atlas en escala de grises con una máscara a un espacio RGB
        :return:
        """
        img_atlas = self.__atlas_corregistrado[self.__corte_actual, ...]
        img_atlas_color = np.zeros((img_atlas.shape[0], img_atlas.shape[1], 3), dtype=np.uint8)
        binary_mask = np.logical_and(np.greater_equal(img_atlas, self.__region_atlas_mostrar),
                                     np.less_equal(img_atlas, self.__region_atlas_end))

        for i, c in enumerate(self.__color):
            region_color = img_atlas_color[:, :, i]
            region_color[binary_mask] = c
            img_atlas_color[:, :, i] = region_color
        return img_atlas_color

    def get_corte_actual_pixels(self):
        img = cv2.resize(self.__img_base[self.__corte_actual], None,
                         fx=self.__zoom_factor, fy=self.__zoom_factor)
        img = np.clip(img, self.__img_minimum, self.__img_maximo)
        return img

    # Visualización
    def get_valor_minimum_maximo_de_visualization(self):
        return self.__valor_minimum, self.__valor_maximo

    def get_global_minimum_maximo(self):
        return self.__img_minimum, self.__img_maximo

    def get_valor_opacidad(self):
        return self.__valor_opacidad

    def get_referencias_atlas(self):
        return self.__referencia_atlas

    def get_selected_index_reference_atlas(self):
        return self.__region_atlas_mostrar-1, self.__region_atlas_end-1

    def get_current_color(self):
        return self.__color

    # -----------------------------------------------------------------------
    # Set functions
    # -----------------------------------------------------------------------

    def set_atlas_corregistrado(self, atlas):
        self.__atlas_corregistrado = atlas

    def set_paciente_medio_corregistrado(self, paciente):
        self.__paciente_medio_corregistrado = paciente

    def set_corte_actual(self, corte):
        self.__corte_actual = corte

    def set_minimum_maximo_visualization(self, minimum, maximo):
        if minimum > maximo:
            raise ValueError(self.ERROR_MIN_MAX)
        if minimum < self.__img_minimum:
            raise ValueError("El mínimo no puede ser más pequeño que el mínimo global")
        if maximo > self.__img_maximo:
            raise ValueError("El máximo no puede ser más grande que el máximo global")

        self.__valor_minimum = minimum
        self.__valor_maximo = maximo

    def set_valor_opacidad(self, valor: float):
        if valor < 0 or valor > 1:
            raise ValueError("El valor debe ser real y entre 0 y 1")
        self.__valor_opacidad = valor

    def set_index_atlas_mostrar(self, index, end):
        self.__region_atlas_mostrar = int(self.__referencia_atlas[index].split(" ")[0])
        self.__region_atlas_end = int(self.__referencia_atlas[end].split(" ")[0])

    def set_current_color(self, color):
        self.__color = color

    # -----------------------------------------------------------------------
    # Zoom functions
    # -----------------------------------------------------------------------

    def incrementar_zoom(self):
        if self.__zoom_factor < self.ZOOM_MAX:
            self.__zoom_factor += self.ZOOM_PASS

    def decrementar_zoom(self):
        if self.__zoom_factor > self.ZOOM_MIN:
            self.__zoom_factor -= self.ZOOM_PASS

    def reiniciar_zoom(self):
        self.__zoom_factor = 1.0

    # -----------------------------------------------------------------------
    # Load functions
    # -----------------------------------------------------------------------

    @staticmethod
    def __load_files(path, sort_slices=True):
        files = os.listdir(path)
        files.sort(reverse=True)
        slices = [dcmread(f'{path}/{file}') for file in files]
        pixel_len = [slices[0].SliceThickness, slices[0].PixelSpacing[0], slices[0].PixelSpacing[1]]
        if sort_slices:
            # Re-ordenar segons SliceLocation
            assert all(hasattr(slc, 'SliceLocation') for slc in slices)
            slices = sorted(slices, key=lambda s: s.SliceLocation)

        img = np.stack([slc.pixel_array for slc in slices])
        img = np.flip(img, axis=0)  # Per a visualitzar-ho amb la orientació correcta
        return img, pixel_len

    @staticmethod
    def __load_file(path):
        return dcmread(path).pixel_array

    @staticmethod
    def __load_referencia(path):
        with open(path, "r") as f:
            return f.read().splitlines()

    # -----------------------------------------------------------------------
    # Utilidades
    # -----------------------------------------------------------------------
    def escalar_imagen(self, img):
        img_scaled = img.copy()
        img_scaled = ((img_scaled - self.__valor_minimum) / (self.__valor_maximo-self.__valor_minimum)) * 255
        img_scaled = np.clip(img_scaled, 0, 255)
        return img_scaled.astype(np.uint8)
