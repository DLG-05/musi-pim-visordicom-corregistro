from PyQt5.QtGui import QIcon, QCursor
from PyQt5.QtWidgets import QDialog, QToolTip
from PyQt5.uic import loadUi


class OpacityDialog(QDialog):

    def __init__(self, valor, apply_function, parent=None) -> None:
        super().__init__(parent)
        loadUi("./view/ui_files/Opacity_Dialog.ui", self)
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))
        self.__apply_function = apply_function

        self.opacitySlider.setValue(int(valor*100))
        self.opacitySlider.setMinimum(0)
        self.opacitySlider.setMaximum(100)
        self.opacitySlider.setSingleStep(5)
        self.opacitySlider.setTickInterval(5)
        self.opacityLabel.setText("{:1.2f}".format(valor))
        self.opacitySlider.valueChanged.connect(self.__tooltip_show)

    def __tooltip_show(self, value):
        self.opacityLabel.setText("{:1.2f}".format(value/100.0))
        QToolTip.showText(QCursor.pos(), "{:1.2f}".format(value/100.0), self)

    def accept(self) -> None:
        valor = self.opacitySlider.value()/100.0
        self.__apply_function(valor)
        super().accept()
