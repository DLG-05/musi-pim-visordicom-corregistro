from PyQt5.QtGui import QIcon, QCursor
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QToolTip
from PyQt5.uic import loadUi

from view.atlas_form import AtlasDialog
from view.file_form import FileDialog
from view.minimum_maximo_form import MinimumMaximoDialog
from view.opacity_form import OpacityDialog
from view.ui_components.imagelabel import ImageLabel


class MainWindow(QMainWindow):

    def __init__(self, parent=None) -> None:
        super().__init__(parent)
        loadUi("./view/ui_files/Main_Window.ui", self)
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))
        self.__img = None
        self.__selector_de_cortes_function = None
        self.__formulario_minimum_maximo = None
        self.__formulario_abrir_ficheros = None
        self.__formulario_atlas = None
        self.__formulario_opacidad = None
        self.__on_click_image = None

    def set_triggers(self, open_folder, selector_de_cortes, zoom_in, zoom_out, reset_zoom, mostrar_minimum_maximo,
                     on_click_image, corregistrar, conf_atlas, conf_opacity, show_atlas):
        """
        Realiza la asociación de todas las funciones de la interfaz
        """
        self.__on_click_image = on_click_image
        self.actionAbrir.triggered.connect(open_folder)
        self.__selector_de_cortes_function = selector_de_cortes
        self.selectorDeCortesSlicer.valueChanged.connect(self.__tooltip_slider)
        self.actionZoom_In.triggered.connect(zoom_in)
        self.actionZoom_In.setShortcut('Ctrl++')
        self.actionZoom_Out.triggered.connect(zoom_out)
        self.actionZoom_Out.setShortcut('Ctrl+-')
        self.actionReset_Zoom.triggered.connect(reset_zoom)
        self.actionReset_Zoom.setShortcut('Ctrl+0')
        self.selectorDeCortesSlicer.sliderMoved.connect(self.__tooltip_slider)
        self.actionContrastar_Imagen.triggered.connect(mostrar_minimum_maximo)
        self.startCancelButton.clicked.connect(corregistrar)
        self.actionConfigurar_Atlas.triggered.connect(conf_atlas)
        self.actionConfigurar_Opacidad.triggered.connect(conf_opacity)
        self.actionActivar_Atlas.triggered.connect(show_atlas)

    def __tooltip_slider(self, value):
        """
        Agrega un popup al pasar el ratón sobre el slider
        :param value: valor que se debe mostrar
        :return:
        """
        QToolTip.showText(QCursor.pos(), str(value), self)
        self.__selector_de_cortes_function(value)

    def set_imagen(self, imagen):
        """
        Permite mostrar una imagen en la área principal
        :param imagen: Imagen en formato RGB
        :return:
        """
        if self.__img is not None:
            self.__img.deleteLater()
        self.__img = ImageLabel(on_click=self.__on_click_image)
        self.__img.set_image(imagen)
        self.imageScrollArea.setWidget(self.__img)

    def configurar_selector_cortes(self, minimum, maximo):
        """
        Configura los rangos del selector de cortes
        :param minimum: Corte mínimo
        :param maximo: Corte máximo
        :return:
        """
        self.selectorDeCortesSlicer.setEnabled(True)
        self.selectorDeCortesSlicer.setMinimum(minimum)
        self.selectorDeCortesSlicer.setMaximum(maximo)

    def corte_cambiado(self, i, rango):
        """
        Modifica el valor de la etiqueta que muestra el corte
        :param i: corte seleccionado
        :param rango: valor máximo
        :return:
        """
        self.selectorDeCortesValue.setText("{}/{}".format(i, rango))

    def mostrar_mensaje_error(self, mensaje):
        """
        Dialogo para indicar un mensaje de error de manera gráfica
        :param mensaje: Mensaje para mostrar
        :return:
        """
        error = QMessageBox(self)
        error.setText("Error")
        error.setInformativeText(mensaje)
        error.setIcon(QMessageBox.Warning)
        error.setWindowTitle("Error")
        error.exec_()

    def mostrar_formulario_minimum_maximo(self, global_minimum, global_maximum, minimum_value, maximum_value,
                                          return_function):
        self.__formulario_minimum_maximo = MinimumMaximoDialog(global_minimum=global_minimum,
                                                               global_maximo=global_maximum,
                                                               valor_minimum=minimum_value,
                                                               valor_maximum=maximum_value,
                                                               apply_function=return_function,
                                                               parent=None)
        self.__formulario_minimum_maximo.show()

    def mostrar_formulario_abrir_carpeta(self, return_function):
        self.__formulario_abrir_ficheros = FileDialog(return_function, self, None)
        self.__formulario_abrir_ficheros.show()

    def activar_acciones(self):
        self.actionAbrir.setEnabled(True)
        self.actionZoom_In.setEnabled(True)
        self.actionZoom_Out.setEnabled(True)
        self.actionReset_Zoom.setEnabled(True)
        self.actionContrastar_Imagen.setEnabled(True)
        self.actionConfigurar_Opacidad.setEnabled(True)
        self.actionConfigurar_Atlas.setEnabled(True)
        self.actionActivar_Atlas.setEnabled(True)
        self.startCancelButton.setEnabled(True)
        self.actionCorregistro_mediante_cuaterniones.setEnabled(True)

    def desactivar_acciones(self):
        self.actionAbrir.setEnabled(False)
        self.actionZoom_In.setEnabled(False)
        self.actionZoom_Out.setEnabled(False)
        self.actionReset_Zoom.setEnabled(False)
        self.actionContrastar_Imagen.setEnabled(False)
        self.actionConfigurar_Opacidad.setEnabled(False)
        self.actionConfigurar_Atlas.setEnabled(False)
        self.actionActivar_Atlas.setEnabled(False)
        self.startCancelButton.setEnabled(False)
        self.actionCorregistro_mediante_cuaterniones.setEnabled(False)

    def modificar_valores(self, x, y, valor, valor_rgb):
        """
        Se encarga de mostrar los valores de la imagen que se hayan seleccionado
        :param x: coordenada x
        :param y: coordenada y
        :param valor: valor sin convertir
        :param valor_rgb: valor convertido
        :return:
        """
        self.selectedPixelLabel.setText("({},{})".format(x, y))
        self.originalValueLabel.setText(str(valor))
        self.processedValueLabel.setText(str(valor_rgb))

    def mostrar_formulario_atlas(self, callback, atlas, idx_atlas, idx_atlas_end, color):
        self.__formulario_atlas = AtlasDialog(callback, atlas, idx_atlas, idx_atlas_end, color, None)
        self.__formulario_atlas.show()

    def mostrar_dialogo_opacidad(self, valor, callback):
        self.__formulario_opacidad = OpacityDialog(valor, callback)
        self.__formulario_opacidad.show()

    def get_status_atlas(self):
        return self.actionActivar_Atlas.isChecked()

    def get_status_corregistro_cuaterniones(self):
        return self.actionCorregistro_mediante_cuaterniones.isChecked()

    def configurar_barra_progreso(self, minimum, maximo):
        self.progressBar.setMaximum(maximo)
        self.progressBar.setMinimum(minimum)
        self.progressBar.setValue(0)

    def reiniciar_barra_progreso(self):
        self.progressBar.setMaximum(100)
        self.progressBar.setMinimum(0)
        self.progressBar.setValue(0)

    def mostrar_mensaje_informativo(self, mensaje):
        """
        Dialogo para indicar un mensaje de error de manera gráfica
        :param mensaje: Mensaje para mostrar
        :return:
        """
        error = QMessageBox(self)
        error.setText("Información")
        error.setInformativeText(mensaje)
        error.setIcon(QMessageBox.Information)
        error.setWindowTitle("Información")
        error.exec_()
