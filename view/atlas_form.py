from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtWidgets import QDialog, QColorDialog
from PyQt5.uic import loadUi


class AtlasDialog(QDialog):

    def __init__(self, callback, referencias_atlas=[], idx_atlas_default=0,
                 idx_atlas_end_default=0, color_actual=[0, 0, 255], parent=None) -> None:
        super().__init__(parent)
        loadUi("./view/ui_files/Atlas_Dialog.ui", self)
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))

        self.__apply_function = callback

        # Se asignan los valores al comboBox
        self.atlasComboBox.addItems(referencias_atlas)
        self.atlasComboBox.setCurrentIndex(idx_atlas_default)

        self.atlas2ComboBox.addItems(referencias_atlas)
        self.atlas2ComboBox.setCurrentIndex(idx_atlas_end_default)

        # Se asigna el color al botón
        self.__color = QColor.fromRgb(color_actual[0], color_actual[1], color_actual[2])
        self.colorButton.setStyleSheet("background-color: %s;" % self.__color.name())
        self.colorButton.clicked.connect(self.__abrir_dialogo_color)

    def __abrir_dialogo_color(self):
        color = QColorDialog.getColor()
        if color.isValid():
            self.__color = color
            self.colorButton.setStyleSheet("background-color: %s;" % self.__color.name())

    def accept(self) -> None:
        self.__apply_function(self.atlasComboBox.currentIndex(), self.atlas2ComboBox.currentIndex(),
                              self.__color.getRgb()[0:3])
        super().accept()
