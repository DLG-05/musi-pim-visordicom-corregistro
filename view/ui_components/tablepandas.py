from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt


class TablePandas(QAbstractTableModel):

    def __init__(self, data, parent=None) -> None:
        """
        Clase que permite asociar un DataFrame de Pandas a una tabla QT
        :param data:
        :param parent:
        """
        super().__init__(parent)
        self.__data = data

    def data(self, index: QModelIndex, role: int):
        if role == Qt.DisplayRole:
            value = self.__data.iloc[index.row(), index.column()]
            return str(value)

    def rowCount(self, index):
        return self.__data.shape[0]

    def columnCount(self, index):
        return self.__data.shape[1]

    def headerData(self, section, orientation, role):
        # section is the index of the column/row.
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return str(self.__data.columns[section])

            if orientation == Qt.Vertical:
                return str(self.__data.index[section])
