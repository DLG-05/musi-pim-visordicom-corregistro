from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QFileDialog
from PyQt5.uic import loadUi


class FileDialog(QDialog):

    def __init__(self, apply_function, vista: 'MainWindow' = None, parent=None) -> None:
        super().__init__(parent)
        loadUi("./view/ui_files/File_Dialog.ui", self)
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))
        self.__apply_function = apply_function
        self.__path_paciente = './data/RM_Brain_3D-SPGR'
        self.__path_atlas = './data/AAL3_1mm.dcm'
        self.__path_paciente_medio = "./data/icbm_avg_152_t1_tal_nlin_symmetric_VI.dcm"

        self.__path_referencia_atlas = './data/AAL3_1mm.txt'

        self.pacienteEdit.setText(self.__path_paciente)
        self.atlasEdit.setText(self.__path_atlas)
        self.pacienteMedioEdit.setText(self.__path_paciente_medio)
        self.referenciaAtlasEdit.setText(self.__path_referencia_atlas)

        self.__vista = vista

        self.abrirPacienteButton.clicked.connect(self.__abrir_paciente)
        self.abrirAtlasButton.clicked.connect(self.__abrir_atlas)
        self.abrirPacienteMedioButton.clicked.connect(self.__abrir_paciente_medio)
        self.abrirReferenciaAtlasButton.clicked.connect(self.__abrir_referencia_atlas)

    def __abrir_referencia_atlas(self):
        directorio = QFileDialog.getOpenFileName(self, "Seleccione el fichero que tiene las referencias del atlas",
                                                 './',
                                                 "Text Files (*.txt)")
        self.__path_referencia_atlas = directorio[0]
        self.referenciaAtlasEdit.setText(self.__path_referencia_atlas)

    def __abrir_paciente(self):
        directorio = QFileDialog.getExistingDirectory(self, "Seleccione una carpeta con ficheros DICOM")
        self.__path_paciente = directorio
        self.pacienteEdit.setText(directorio)

    def __abrir_atlas(self):
        directorio = QFileDialog.getOpenFileName(self, "Seleccione el fichero que tiene el atlas", './',
                                                 "DICOM (*.dcm)")
        self.__path_atlas = directorio[0]
        self.atlasEdit.setText(self.__path_atlas)

    def __abrir_paciente_medio(self):
        directorio = QFileDialog.getOpenFileName(self, "Seleccione el fichero que tiene el paciente medio", './',
                                                 "DICOM (*.dcm)")
        self.__path_paciente_medio = directorio[0]
        self.pacienteMedioEdit.setText(self.__path_paciente_medio)

    def accept(self) -> None:
        if self.__path_paciente_medio is not None and self.__path_paciente is not None \
                and self.__path_atlas is not None and self.__path_referencia_atlas is not None:
            self.__apply_function(self.__path_paciente, self.__path_atlas, self.__path_paciente_medio,
                                  self.__path_referencia_atlas)
            super().accept()
        else:
            self.__vista.mostrar_mensaje_error("Seleccione los 3 ficheros")
