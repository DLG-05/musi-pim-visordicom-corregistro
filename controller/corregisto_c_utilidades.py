import math

import numpy as np


def mean_square_error(img_referencia, img_entrada):
    """Devuelve un array con los residuos cuadráticos del ajuste."""
    residuos = np.sqrt((img_referencia - img_entrada) ** 2)
    residuos = residuos.flatten()
    return residuos


def traslacion(punto, vector_traslacion):
    x, y, z = punto
    t_1, t_2, t_3 = vector_traslacion
    punto_transformado = (x + t_1, y + t_2, z + t_3)
    return punto_transformado


def rotacion_axial(punto, angulo_en_radianes, eje_traslacion):
    x, y, z = punto
    v_1, v_2, v_3 = eje_traslacion
    #   Vamos a normalizarlo para evitar introducir restricciones en el optimizador
    v_norm = math.sqrt(sum([coord ** 2 for coord in [v_1, v_2, v_3]]))
    v_1, v_2, v_3 = v_1 / v_norm, v_2 / v_norm, v_3 / v_norm
    #   Calcula cuaternión del punto
    p = (0, x, y, z)
    #   Calcula cuaternión de la rotación
    cos, sin = math.cos(angulo_en_radianes / 2), math.sin(angulo_en_radianes / 2)
    q = (cos, sin * v_1, sin * v_2, sin * v_3)
    #   Calcula el conjugado
    q_conjugado = (cos, -sin * v_1, -sin * v_2, -sin * v_3)
    #   Calcula el cuaternión correspondiente al punto rotado
    p_prima = multiplicar_quaterniones(q, multiplicar_quaterniones(p, q_conjugado))
    # Devuelve el punto rotado
    punto_transformado = p_prima[1], p_prima[2], p_prima[3]
    return punto_transformado


def transformacion_rigida_3D(punto, parametros):
    x, y, z = punto
    t_11, t_12, t_13, alpha_in_rad, v_1, v_2, v_3 = parametros
    #   Aplicar una primera traslación
    x, y, z = traslacion(punto=(x, y, z), vector_traslacion=(t_11, t_12, t_13))
    #   Aplicar una rotación axial traslación
    x, y, z = rotacion_axial(punto=(x, y, z), angulo_en_radianes=alpha_in_rad, eje_traslacion=(v_1, v_2, v_3))
    punto_transformado = (x, y, z)
    return punto_transformado


def inv_transformacion_rigida_3D(punto, parametros):
    x, y, z = punto
    t_11, t_12, t_13, alpha_in_rad, v_1, v_2, v_3 = parametros
    #   Aplicar una rotación axial traslación
    x, y, z = rotacion_axial(punto=(x, y, z), angulo_en_radianes=-alpha_in_rad, eje_traslacion=(v_1, v_2, v_3))
    #   Aplicar una primera traslación
    x, y, z = traslacion(punto=(x, y, z), vector_traslacion=(-t_11, -t_12, -t_13))
    punto_transformado = (x, y, z)
    return punto_transformado


def multiplicar_quaterniones(q1, q2):
    """Multiplica cuaterniones expresados como (1, i, j, k)."""
    return (
        q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2] - q1[3] * q2[3],
        q1[0] * q2[1] + q1[1] * q2[0] + q1[2] * q2[3] - q1[3] * q2[2],
        q1[0] * q2[2] - q1[1] * q2[3] + q1[2] * q2[0] + q1[3] * q2[1],
        q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1] + q1[3] * q2[0]
    )


def cuaternion_conjugado(q):
    """Conjuga un cuaternión expresado como (1, i, j, k)."""
    return q[0], -q[1], -q[2], -q[3]


def generar_img_malla(img, size_malla):
    size_of = np.array(img.shape)
    size_of = size_of / size_malla
    size_of = np.ceil(size_of).astype('int')

    # Se obtienen los límites de la imagen original
    lim_x, lim_y, lim_z = img.shape

    # Se genera la imagen malla
    img_malla = np.zeros(size_of)

    for i, x in enumerate(range(0, lim_x, size_malla)):
        for j, y in enumerate(range(0, lim_y, size_malla)):
            for k, z in enumerate(range(0, lim_z, size_malla)):
                if x < 0 or y < 0 or z < 0 or x >= lim_x or y >= lim_y or z >= lim_z:
                    img_malla[i, j, k] = 0
                else:
                    img_malla[i, j, k] = img[x, y, z]

    return img_malla


def transformar_imagen(img, size_malla, parameters):
    size_of = np.array(img.shape)
    size_of = size_of / size_malla
    size_of = np.ceil(size_of).astype('int')

    # Se obtienen los límites de la imagen original
    lim_x, lim_y, lim_z = img.shape

    # Se genera la imagen malla
    img_malla = np.zeros(size_of)

    for i, v_x in enumerate(range(0, lim_x, size_malla)):
        for j, v_y in enumerate(range(0, lim_y, size_malla)):
            for k, v_z in enumerate(range(0, lim_z, size_malla)):
                punto = (v_x, v_y, v_z)  # 0,0,0 - 5,5,5 - 10,10,10...
                punto_transformado = transformacion_rigida_3D(punto, parameters)  # Se aplica la transformación
                x, y, z = np.array(punto_transformado).round().astype(int)  # Se busca el punto entero más cercano
                # Se comprueba si está dentro o fuera de los límites
                if x < 0 or y < 0 or z < 0 or x >= lim_x or y >= lim_y or z >= lim_z:
                    img_malla[i, j, k] = 0
                else:
                    img_malla[i, j, k] = img[x, y, z]

    return img_malla


def destransformar_imagen(img, parameters):
    img_inv = np.zeros_like(img)

    lim_x, lim_y, lim_z = img.shape

    for i in range(lim_x):
        for j in range(lim_y):
            for k in range(lim_z):
                punto = (i, j, k)
                punto_inverso = inv_transformacion_rigida_3D(punto, parameters)
                x, y, z = np.array(punto_inverso).round().astype(int)  # Se busca el punto entero más cercano
                # Se comprueba si está dentro o fuera de los límites
                if x < 0 or y < 0 or z < 0 or x >= lim_x or y >= lim_y or z >= lim_z:
                    img_inv[i, j, k] = 0
                else:
                    img_inv[i, j, k] = img[x, y, z]

    return img_inv
