from scipy import ndimage as nd


def transformacion(img_inp, parameters):
    tx, ty, tz, alpha, beta, gamma = parameters
    # Se realizan las rotaciones mediante los ángulos de euler
    # Se realiza mediante el vecino más cercano y mediante una interpolación lineal de 3D
    img_alpha = nd.rotate(img_inp, alpha, axes=(0, 1), reshape=False,  mode='nearest', order=1)
    img_alpha = nd.rotate(img_alpha, beta, axes=(1, 2), reshape=False,  mode='nearest', order=1)
    img_alpha = nd.rotate(img_alpha, gamma, axes=(0, 1), reshape=False,  mode='nearest',  order=1)
    img_mod = nd.shift(img_alpha, (tx, ty, tz), order=1, mode='nearest')
    return img_mod


def transformacion_inversa(img_ref, parameters):
    tx, ty, tz, alpha, beta, gamma = parameters
    img_alpha = nd.shift(img_ref, (-tx, -ty, -tz), order=1, mode='nearest')
    img_alpha = nd.rotate(img_alpha, -gamma, axes=(0, 1), reshape=False, mode='nearest', order=1)
    img_alpha = nd.rotate(img_alpha, -beta, axes=(1, 2), reshape=False, mode='nearest', order=1)
    img_alpha = nd.rotate(img_alpha, -alpha, axes=(0, 1), reshape=False, mode='nearest', order=1)
    return img_alpha