from PyQt5.QtCore import QObject, pyqtSignal
from scipy.optimize import least_squares

import controller.corregistro_e_utilidades as ut_e
import controller.corregisto_c_utilidades as ut
from model.dicom_model import DicomModel
from scipy import ndimage as nd
import numpy as np
from matplotlib import pyplot as plt


class AlgoritmoCorregistroEuler(QObject):

    finished = pyqtSignal()
    tiempo = pyqtSignal(float)
    par = pyqtSignal(list)

    def __init__(self, modelo: DicomModel, parent=None) -> None:
        super().__init__(parent)
        self.__modelo = modelo
        # Parámetros de transformación
        self.__parameters = [0, 0, 0,  # Traslación X Y Z
                             0, 0, 0]  # Rotación alpha beta gamma

        self.__range_parameters = [3, 3, 3, 8, 8, 8]

        self.__img_ref = self.__modelo.get_paciente_medio_normalizado()
        self.__img_inp = self.__modelo.get_paciente_normalizado()

        # Basado en la visualización del gaussiano, 5 o superior se pierden las estructuras
        self.__img_inp = nd.gaussian_filter(self.__img_inp, sigma=3)

        # Se realizan adaptaciones para que el paciente tenga las mismas dimensiones que el paciente medio
        self.__img_inp_mod = self.__img_inp[20:328, 15:-16, 33:-34]
        self.__img_inp = nd.zoom(self.__img_inp_mod,
                                 zoom=np.array(self.__img_ref.shape) / np.array(self.__img_inp_mod.shape), order=1)

    def function_a_minimizar(self, parameters):
        img_mod = ut_e.transformacion(self.__img_inp, parameters)
        mse = ut.mean_square_error(self.__img_ref, img_mod)
        return mse

    @staticmethod
    def deshacer_escalados_cropping(img_inp_original_crop, img_inp_original, img_ref):
        img_ref_mod = nd.zoom(img_ref, zoom=np.array(img_inp_original_crop.shape) / np.array(img_ref.shape), order=1)
        img_ref_mod_shape = img_inp_original.shape
        img_ref_mod_padding = np.zeros(img_ref_mod_shape, dtype=img_ref_mod.dtype)
        img_ref_mod_padding[20:328, 15:-16, 33:-34] = img_ref_mod
        return img_ref_mod_padding

    def run(self):
        # invocar al optimizador
        resultado = least_squares(self.function_a_minimizar,
                                  x0=self.__parameters,
                                  verbose=2, method='trf', gtol=1e-10, xtol=1e-11,
                                  diff_step=self.__range_parameters)
        x_opt = resultado.x
        print(x_opt)

        # x_opt = [9.45450445, 0.11040768, -4.27587097, 3.28131182, 3.01069116, 3.26913013]

        print("Transformación inversa")
        # Realizar la transformación inversa
        p_medio = ut_e.transformacion_inversa(self.__img_ref, x_opt)
        atlas = ut_e.transformacion_inversa(self.__modelo.get_atlas(), x_opt)
        p_medio = self.deshacer_escalados_cropping(self.__img_inp_mod, self.__modelo.get_paciente(), p_medio)
        atlas = self.deshacer_escalados_cropping(self.__img_inp_mod, self.__modelo.get_paciente(), atlas)
        self.__modelo.set_atlas_corregistrado(atlas)
        self.__modelo.set_paciente_medio_corregistrado(p_medio)
        print("Fin de transformación inversa")
        self.par.emit(list(x_opt))
        self.finished.emit()
