import math

from PyQt5.QtCore import QObject, pyqtSignal
from scipy.optimize import least_squares

from controller.corregisto_c_utilidades import generar_img_malla, transformar_imagen, mean_square_error, \
    destransformar_imagen
from model.dicom_model import DicomModel
from scipy import ndimage as nd
import numpy as np


class AlgoritmoCorregistroCuater(QObject):
    par = pyqtSignal(list)
    finished = pyqtSignal()

    SIZE_MALLA = 5

    def __init__(self, modelo: DicomModel, parent=None):
        super().__init__(parent)
        self.__modelo = modelo

        self.__parameters = [0, 0, 0,  # Traslación X Y Z
                             0, 1, 0, 0]  # Rotación ángulo, vector
        self.__range_parameters = [3, 3, 3, 0.14, 0.1, 0.1, 0.1]

        self.__img_ref = self.__modelo.get_paciente_medio_normalizado()
        self.__img_inp = self.__modelo.get_paciente_normalizado()

        # Basado en la visualización del gaussiano, 5 o superior se pierden las estructuras
        self.__img_inp = nd.gaussian_filter(self.__img_inp, sigma=3)

        # Se realizan adaptaciones para que el paciente tenga las mismas dimensiones que el paciente medio
        self.__img_inp_mod = self.__img_inp[20:328, 15:-16, 33:-34]
        self.__img_inp = nd.zoom(self.__img_inp_mod,
                                 zoom=np.array(self.__img_ref.shape) / np.array(self.__img_inp_mod.shape), order=1)

        self.__img_ref_malla = generar_img_malla(self.__img_ref, self.SIZE_MALLA)

    @staticmethod
    def deshacer_escalados_cropping(img_inp_original_crop, img_inp_original, img_ref):
        img_ref_mod = nd.zoom(img_ref, zoom=np.array(img_inp_original_crop.shape) / np.array(img_ref.shape), order=1)
        img_ref_mod_shape = img_inp_original.shape
        img_ref_mod_padding = np.zeros(img_ref_mod_shape, dtype=img_ref_mod.dtype)
        img_ref_mod_padding[20:328, 15:-16, 33:-34] = img_ref_mod
        return img_ref_mod_padding

    def function_a_minimizar(self, par):
        img_transformada = transformar_imagen(self.__img_inp, self.SIZE_MALLA, par)
        mse = mean_square_error(self.__img_ref_malla, img_transformada)
        return mse

    def run(self):
        resultado = least_squares(self.function_a_minimizar, x0=self.__parameters, verbose=2, method='trf',
                                  gtol=1e-10, diff_step=self.__range_parameters)
        x_opt = resultado.x
        print(x_opt)

        print("Transformación inversa")
        # Realizar la transformación inversa
        p_medio = destransformar_imagen(self.__img_ref, x_opt)
        atlas = destransformar_imagen(self.__modelo.get_atlas(), x_opt)
        p_medio = self.deshacer_escalados_cropping(self.__img_inp_mod, self.__modelo.get_paciente(), p_medio)
        atlas = self.deshacer_escalados_cropping(self.__img_inp_mod, self.__modelo.get_paciente(), atlas)
        self.__modelo.set_atlas_corregistrado(atlas)
        self.__modelo.set_paciente_medio_corregistrado(p_medio)
        print("Fin de transformación inversa")
        self.par.emit(list(x_opt))
        self.finished.emit()

#    run()