from PyQt5.QtCore import QThread

from controller.algoritmo_corregistro_cuaterniones import AlgoritmoCorregistroCuater
from controller.algoritmo_corregistro_euler import AlgoritmoCorregistroEuler
from model.dicom_model import DicomModel
from view.main_window import MainWindow
from typing import Optional


class Controller:

    def __init__(self, vista: MainWindow) -> None:
        super().__init__()
        self.__vista = vista
        self.__set_triggers_vista()
        self.__modelo: Optional[DicomModel] = None
        # Para activar o desactivar el click de la pantalla
        # Para la segmentación en paralelo para no bloquear el programa
        self.__thread: Optional[QThread] = None
        self.__worker = None

    def __set_triggers_vista(self):
        """
        Realiza la asociación de los eventos de la vista con los métodos del controlador
        :return:
        """
        self.__vista.set_triggers(open_folder=self.__open_folder,
                                  selector_de_cortes=self.__selector_de_cortes,
                                  zoom_in=self.__zoom_in,
                                  zoom_out=self.__zoom_out,
                                  reset_zoom=self.__reiniciar_zoom,
                                  mostrar_minimum_maximo=self.__mostrar_dialogo_minimum_maximo,
                                  on_click_image=self.__on_click_imagen,
                                  corregistrar=self.__corregistrar,
                                  conf_atlas=self.__mostrar_formulario_atlas,
                                  conf_opacity=self.__mostrar_opacidad,
                                  show_atlas=self.__show_atlas
                                  )

    def __show_atlas(self, status):
        self.__mostrar_imagen()

    def __selector_de_cortes(self, valor):
        """
        Método que se encarga de cambiar el corte según el valor del slider.
        :param valor: Corte seleccionado 1-n y lo convierte a 0-n-1
        :return:
        """
        self.__modelo.set_corte_actual(valor - 1)
        self.__vista.corte_cambiado(valor, self.__modelo.get_numero_de_cortes())
        # self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb())
        self.__mostrar_imagen()

    def __open_folder(self):
        """
        Se encarga de mostrar el dialogo para abrir una carpeta con ficheros DICOM
        :return:
        """
        self.__vista.mostrar_formulario_abrir_carpeta(self.__generar_modelo)

    def __generar_modelo(self, path_paciente, path_atlas, path_paciente_medio, path_referencia_atlas):
        try:
            self.__modelo = DicomModel(path_paciente, path_atlas, path_paciente_medio, path_referencia_atlas)
            # self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb())
            self.__mostrar_imagen()
            self.__vista.configurar_selector_cortes(1, self.__modelo.get_numero_de_cortes())
            self.__vista.corte_cambiado(1, self.__modelo.get_numero_de_cortes())
            self.__vista.activar_acciones()
        except ValueError as err:
            self.__vista.mostrar_mensaje_error(err.args[0])

    # ------------------------------------
    # Zoom utilities
    # ------------------------------------

    def __zoom_in(self):
        """
        Se encarga de ampliar la imagen
        :return:
        """
        self.__modelo.incrementar_zoom()
        # self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb())
        self.__mostrar_imagen()

    def __zoom_out(self):
        """
        Se encarga de reducir la imagen
        :return:
        """
        self.__modelo.decrementar_zoom()
        # self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb())
        self.__mostrar_imagen()

    def __reiniciar_zoom(self):
        """Se encarga de mostrar la imagen a tamaño natural"""
        self.__modelo.reiniciar_zoom()
        # self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb())
        self.__mostrar_imagen()

    # --------------------------------------
    # Configurar valor máximo y mínimo de visualización
    # --------------------------------------

    def __mostrar_dialogo_minimum_maximo(self):
        """
        Se encarga de preparar el dialogo para poder realizar el cambio de valores
        :return:
        """
        global_minimum, global_maximum = self.__modelo.get_global_minimum_maximo()
        valor_minimum, valor_maximum = self.__modelo.get_valor_minimum_maximo_de_visualization()
        self.__vista.mostrar_formulario_minimum_maximo(global_minimum, global_maximum, valor_minimum, valor_maximum,
                                                       self.__cambiar_minimum_maximum)

    def __cambiar_minimum_maximum(self, valor_minimum, valor_maximum):
        """
        Se encarga de aplicar el proceso de cambio de visualización
        :param valor_minimum: valor mínimo deseado
        :param valor_maximum: valor máximo deseado
        :return:
        """
        try:
            self.__modelo.set_minimum_maximo_visualization(valor_minimum, valor_maximum)
            self.__mostrar_imagen()
        except ValueError as e:
            self.__vista.mostrar_mensaje_error(e.args[0])

    # --------------------------------------
    # Utilidades
    # --------------------------------------

    def __mostrar_imagen(self):
        self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(self.__vista.get_status_atlas()))

    # --------------------------------------
    # Click Imagen
    # --------------------------------------

    def __on_click_imagen(self, x, y):
        """
        Método que muestra los valores de un pixel concreto de la imagen
        :param x: Coordenada X
        :param y: Coordenada Y
        :return:
        """

        if self.__modelo is not None:
            valor_rgb = self.__modelo.get_corte_actual_rgb()[y, x]
            valor = self.__modelo.get_corte_actual_pixels()[y, x]
            self.__vista.modificar_valores(x, y, valor, valor_rgb[0])

    def __corregistrar(self):
        pass
        self.__thread = QThread()
        if self.__vista.get_status_corregistro_cuaterniones():
            self.__worker = AlgoritmoCorregistroCuater(self.__modelo)
            self.__worker.par.connect(self.__mostrar_resultados_cuaterniones)
            self.__worker.moveToThread(self.__thread)
        else:
            self.__worker = AlgoritmoCorregistroEuler(self.__modelo)
            self.__worker.par.connect(self.__mostrar_resultados)
            self.__worker.moveToThread(self.__thread)

        self.__worker.finished.connect(self.__thread.quit)
        self.__worker.finished.connect(self.__worker.deleteLater)
        self.__thread.finished.connect(self.__activar_acciones)
        self.__thread.finished.connect(self.__mostrar_imagen)
        self.__thread.finished.connect(self.__thread.deleteLater)

        self.__thread.started.connect(self.__desactivar_acciones)
        self.__thread.started.connect(self.__worker.run)

        self.__thread.start()

    def __mostrar_resultados(self, array):
        tx, ty, tz, alpha, beta, gamma = array
        self.__vista.mostrar_mensaje_informativo("Los parámetros obtenidos són los siguientes\n" +
                                                 "Traslación X: {}\n".format(round(tx, 3)) +
                                                 "Traslación Y: {}\n".format(round(ty, 3)) +
                                                 "Traslación Z: {}\n".format(round(tz, 3)) +
                                                 "Rotación alpha: {}º\n".format(round(alpha, 3)) +
                                                 "Rotación beta: {}º\n".format(round(beta, 3)) +
                                                 "Rotación gamma: {}º\n".format(round(gamma, 3))
                                                 )

    def __mostrar_resultados_cuaterniones(self, array):
        tx, ty, tz, angle, v1, v2, v3 = array
        self.__vista.mostrar_mensaje_informativo("Los parámetros obtenidos són los siguientes\n" +
                                                 "Traslación X: {}\n".format(round(tx, 3)) +
                                                 "Traslación Y: {}\n".format(round(ty, 3)) +
                                                 "Traslación Z: {}\n".format(round(tz, 3)) +
                                                 "Rotación ángulo: {} rad\n".format(round(angle, 3)) +
                                                 "Vector de rotación: {}\n".format((round(v1, 3),
                                                                                    round(v2, 3), round(v3, 3)))
                                                 )

    def __desactivar_acciones(self):
        self.__vista.configurar_barra_progreso(0, 0)
        self.__vista.desactivar_acciones()

    def __activar_acciones(self):
        self.__vista.activar_acciones()
        self.__vista.reiniciar_barra_progreso()

    def __mostrar_formulario_atlas(self):
        self.__vista.mostrar_formulario_atlas(self.__configurar_atlas, self.__modelo.get_referencias_atlas(),
                                              self.__modelo.get_selected_index_reference_atlas()[0],
                                              self.__modelo.get_selected_index_reference_atlas()[1],
                                              self.__modelo.get_current_color())

    def __configurar_atlas(self, idx_atlas, idx_atlas_end, color):
        self.__modelo.set_index_atlas_mostrar(idx_atlas, idx_atlas_end)
        self.__modelo.set_current_color(color)
        self.__mostrar_imagen()

    # ---------------------------------
    # Opacidad
    # ---------------------------------

    def __mostrar_opacidad(self):
        self.__vista.mostrar_dialogo_opacidad(self.__modelo.get_valor_opacidad(), self.__configurar_opacidad)

    def __configurar_opacidad(self, valor: float):
        try:
            self.__modelo.set_valor_opacidad(valor)
            self.__mostrar_imagen()
        except ValueError as e:
            self.__vista.mostrar_mensaje_error(e.args[0])
