# MUSI-PIM-VisorDICOM

Este proyecto es una aplicación con intefaz gráfica que permite visualizar una imagen dicom y realizar un corregistro rígido sobre la misma.

El código ha sido desarrollado utilizando un entorno de trabajo ```conda``` sobre Ubuntu.

Para poder instalar el entorno y ejecutar la aplicación hay que realizar los siguientes pasos:

* Disponer de un ordenador con Ubuntu 18.04.
* Tener Anaconda 3 o Miniconda 3 configurado e instalado
* En una terminal que tenga acceso al comando ```conda``` realizar las siguientes operaciones:

```bash
conda env create --file env.yml
conda activate dicom-viewer
python main.py
```

