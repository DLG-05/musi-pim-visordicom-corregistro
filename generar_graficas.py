from matplotlib import pyplot as plt

from controller.corregisto_c_utilidades import mean_square_error, generar_img_malla, transformar_imagen
from controller.corregistro_e_utilidades import transformacion
from model.dicom_model import DicomModel
from scipy import ndimage as nd
import numpy as np

path_paciente = './data/RM_Brain_3D-SPGR'
path_atlas = './data/AAL3_1mm.dcm'
path_paciente_medio = "./data/icbm_avg_152_t1_tal_nlin_symmetric_VI.dcm"
path_paciente_medio_alt = "./data/icbm_avg_152_t1_tal_nlin_symmetric_VI_alternative.dcm"
path_ref = './data/AAL3_1mm.txt'

modelo = DicomModel(path_paciente, path_atlas, path_paciente_medio, path_ref)

atlas = modelo.get_atlas()
paciente_medio = modelo.get_paciente_medio()
paciente = modelo.get_paciente()
paciente_sin_escalar = modelo.get_paciente_sin_escalar()

paciente_adaptado = modelo.get_paciente_normalizado()[20:328, 15:-16, 33:-34]
paciente_adaptado = nd.zoom(paciente_adaptado, zoom=np.array(paciente_medio.shape) / np.array(paciente_adaptado.shape),
                            order=1)
paciente_medio_normalizado = modelo.get_paciente_medio_normalizado()


def mostrar_atlas_paciente_medio():
    for i in range(60, 120):
        plt.subplot(1, 3, 1)
        plt.imshow(paciente_medio[i], cmap='gray')
        plt.subplot(1, 3, 2)
        plt.imshow(atlas[i], cmap='Pastel1')
        plt.subplot(1, 3, 3)
        plt.imshow(paciente_medio[i], cmap='gray', alpha=0.8)
        plt.imshow(atlas[i], cmap='Pastel1', alpha=0.2)
        plt.savefig('./data/resultados/atlas/{}.png'.format(i))


def mostrar_adaptacion_paciente_a_paciente_medio():
    plt.subplot(4, 3, 1)
    plt.imshow(paciente_sin_escalar[paciente_sin_escalar.shape[0] // 2], cmap='gray')
    plt.subplot(4, 3, 2)
    plt.imshow(paciente_sin_escalar[:, paciente_sin_escalar.shape[1] // 2, :], cmap='gray')
    plt.subplot(4, 3, 3)
    plt.imshow(paciente_sin_escalar[:, :, paciente_sin_escalar.shape[2] // 2], cmap='gray')

    plt.subplot(4, 3, 4)
    plt.imshow(paciente[paciente.shape[0] // 2], cmap='gray')
    plt.subplot(4, 3, 5)
    plt.imshow(paciente[:, paciente.shape[1] // 2, :], cmap='gray')
    plt.subplot(4, 3, 6)
    plt.imshow(paciente[:, :, paciente.shape[2] // 2], cmap='gray')

    paciente1 = paciente[20:328, 15:-16, 33:-34]
    paciente1 = nd.zoom(paciente1, zoom=(192 / 300, 1, 1), order=1)
    plt.subplot(4, 3, 7)
    plt.imshow(paciente1[paciente1.shape[0] // 2], cmap='gray')
    plt.subplot(4, 3, 8)
    plt.imshow(paciente1[:, paciente1.shape[1] // 2, :], cmap='gray')
    plt.subplot(4, 3, 9)
    plt.imshow(paciente1[:, :, paciente1.shape[2] // 2], cmap='gray')

    plt.subplot(4, 3, 10)
    plt.imshow(paciente_medio[paciente_medio.shape[0] // 2], cmap='gray')
    plt.subplot(4, 3, 11)
    plt.imshow(paciente_medio[:, paciente_medio.shape[1] // 2, :], cmap='gray')
    plt.subplot(4, 3, 12)
    plt.imshow(paciente_medio[:, :, paciente_medio.shape[2] // 2], cmap='gray')
    plt.savefig('./data/resultados/adaptación/adaptación.png')


def variation_parameters_euler(suavizado=True):
    translation_range = np.linspace(-3, 3, 50)
    rotation_range = np.linspace(-15, 15, 120)
    parameters = [0, 0, 0, 0, 0, 0]
    pruebas = [("traslación x", 0, translation_range),
               ("traslación y", 1, translation_range),
               ("traslación z", 2, translation_range),
               ("Rotación alpha", 3, rotation_range),
               ("Rotación beta", 4, rotation_range),
               ("Rotación gamma", 5, rotation_range)]

    plt.figure(figsize=(15, 7))
    for nombre, i, rango in pruebas:
        print(nombre)
        parameters = [0, 0, 0, 0, 0, 0]
        ax = plt.subplot(2, 3, i + 1)
        ax.set_title(nombre)
        resultados = []
        for j, valor in enumerate(rango):
            if j % 10 == 0:
                print(j)
            parameters[i] = valor
            if suavizado:
                img_transf = transformacion(nd.gaussian_filter(paciente_adaptado, sigma=3), parameters)
            else:
                img_transf = transformacion(paciente_adaptado, parameters)
            mse = mean_square_error(paciente_medio_normalizado, img_transf).mean()
            resultados.append(mse)
        ax.plot(rango, resultados)

    if suavizado:
        plt.savefig('./data/resultados/mse2.png')
    else:
        plt.savefig('./data/resultados/mse1.png')


def variation_parameters_cuaterniones(suavizado=True):
    translation_range = np.linspace(-3, 3, 50)
    rotation_range = np.linspace(-0.27, 0.27, 120)
    parameters = [0, 0, 0, 0, 1, 0, 0]
    pruebas = [("traslación x", 0, translation_range),
               ("traslación y", 1, translation_range),
               ("traslación z", 2, translation_range),
               ("Rotación", 3, rotation_range)]
    SIZE_MALLA = 5
    img_ref_malla = generar_img_malla(paciente_medio_normalizado, SIZE_MALLA)

    plt.figure(figsize=(15, 7))
    for nombre, i, rango in pruebas:
        print(nombre)
        parameters = [0, 0, 0, 0, 1, 0, 0]
        ax = plt.subplot(2, 2, i + 1)
        ax.set_title(nombre)
        resultados = []
        for j, valor in enumerate(rango):
            if j % 10 == 0:
                print(j)
            parameters[i] = valor
            if suavizado:
                img_transf = transformar_imagen(nd.gaussian_filter(paciente_adaptado, sigma=3), SIZE_MALLA, parameters)
            else:
                img_transf = transformar_imagen(paciente_adaptado, SIZE_MALLA, parameters)
            mse = mean_square_error(img_ref_malla, img_transf).mean()
            resultados.append(mse)
        ax.plot(rango, resultados)

    if suavizado:
        plt.savefig('./data/resultados/mse_cuaterniones2.png')
    else:
        plt.savefig('./data/resultados/mse_cuaterniones1.png')


def pruebas_factor_escala():
    pruebas = [(2, 0.5078, 0.5078), (0.5078, 2, 0.5078), (0.5078, 0.5078, 2),
               (1.5, 0.5078, 0.5078), (0.5078, 1.5, 0.5078), (0.5078, 0.5078, 1.5)]

    plt.figure(figsize=(20, 20))
    for i, prueba in enumerate(pruebas):
        paciente_escalado = nd.zoom(paciente_sin_escalar, zoom=prueba, order=1)
        ax = plt.subplot(6, 3, (i * 3) + 1)
        ax.set_title(prueba)
        plt.imshow(paciente_escalado[paciente_escalado.shape[0] // 2], cmap='gray')
        plt.subplot(6, 3, (i * 3) + 2)
        plt.imshow(paciente_escalado[:, paciente_escalado.shape[1] // 2, :], cmap='gray')
        plt.subplot(6, 3, (i * 3) + 3)
        plt.imshow(paciente_escalado[:, :, paciente_escalado.shape[2] // 2], cmap='gray')

    plt.savefig("./data/prueba_factores_escala2.png")


def pruebas_2():
    paciente1 = paciente[20: 210, 15:-16, 33:-34]
    plt.subplot(2, 3, 1)
    plt.imshow(paciente1[paciente1.shape[0] // 2], cmap='gray')
    plt.subplot(2, 3, 2)
    plt.imshow(paciente1[:, paciente1.shape[1] // 2, :], cmap='gray')
    plt.subplot(2, 3, 3)
    plt.imshow(paciente1[:, :, paciente1.shape[2] // 2], cmap='gray')

    print(paciente1.shape)

    plt.subplot(2, 3, 4)
    plt.imshow(paciente_medio[paciente_medio.shape[0] // 2], cmap='gray')
    plt.subplot(2, 3, 5)
    plt.imshow(paciente_medio[:, paciente_medio.shape[1] // 2, :], cmap='gray')
    plt.subplot(2, 3, 6)
    plt.imshow(paciente_medio[:, :, paciente_medio.shape[2] // 2], cmap='gray')
    print(paciente_medio.shape)
    plt.savefig('./data/resultados/adaptación/adaptación2.png')


def prueba_sigma():
    prueba = [1.5, 2, 3, 5, 7, 9]
    plt.figure(figsize=(15, 7))
    for i, valor in enumerate(prueba):
        ax = plt.subplot(2,3, i+1)
        ax.imshow(
            nd.gaussian_filter(modelo.get_paciente_normalizado(),
                               sigma=valor)[modelo.get_paciente_normalizado().shape[0]//2])
        ax.set_title('Gaussian Filter sigma: {}'.format(valor))
    plt.savefig('./data/gaussian.png')


def corregistro_euler_aplicado():

    parameters = [9.45450445, 0.11040768, -4.27587097, 3.28131182, 3.01069116, 3.26913013]

    img_paciente_mod = transformacion(paciente_adaptado, parameters)

    for i in range(125, 144):
        ax = plt.subplot(1,3,1)
        ax.set_title("Paciente sin corregistrar")
        ax.imshow(paciente_adaptado[i], cmap='gray')
        ax = plt.subplot(1, 3, 2)
        ax.set_title("Paciente corregistrado")
        ax.imshow(img_paciente_mod[i], cmap='gray')
        ax = plt.subplot(1, 3, 3)
        ax.set_title("Paciente medio")
        ax.imshow(paciente_medio_normalizado[i], cmap='gray')
        plt.savefig('./data/resultados/corregistro/{}.png'.format(i))


def comprobacion_de_orientacion():
    plt.figure(figsize=(15, 10))
    ax = plt.subplot(3, 3, 1)
    ax.set_title("Paciente anónimo")
    ax.imshow(paciente_adaptado[paciente_adaptado.shape[0]//2], cmap='gray')
    ax = plt.subplot(3, 3, 2)
    ax.imshow(paciente_adaptado[:, paciente_adaptado.shape[1] // 2, :], cmap='gray')
    ax = plt.subplot(3, 3, 3)
    ax.imshow(paciente_adaptado[:, :, paciente_adaptado.shape[2] // 2], cmap='gray')

    ax = plt.subplot(3, 3, 4)
    ax.set_title("Paciente medio")
    ax.imshow(paciente_medio_normalizado[paciente_medio_normalizado.shape[0] // 2], cmap='gray')
    ax = plt.subplot(3, 3, 5)
    ax.imshow(paciente_medio_normalizado[:, paciente_medio_normalizado.shape[1] // 2, :], cmap='gray')
    ax = plt.subplot(3, 3, 6)
    ax.imshow(paciente_medio_normalizado[:, :, paciente_medio_normalizado.shape[2] // 2], cmap='gray')

    ax = plt.subplot(3, 3, 7)
    ax.set_title("Atlas")
    ax.imshow(atlas[atlas.shape[0] // 2], cmap='Pastel1')
    ax = plt.subplot(3, 3, 8)
    ax.imshow(atlas[:, atlas.shape[1] // 2, :], cmap='Pastel1')
    ax = plt.subplot(3, 3, 9)
    ax.imshow(atlas[:, :, atlas.shape[2] // 2], cmap='Pastel1')
    plt.savefig('./data/resultados/comprobación_de_orientación.png')

# mostrar_atlas_paciente_medio()
# mostrar_adaptacion_paciente_a_paciente_medio()
# variation_parameters_euler()
# pruebas_factor_escala()
# pruebas_2()
# prueba_sigma()
# variation_parameters_cuaterniones()
# comprobacion_de_orientacion()
# corregistro_euler_aplicado()
